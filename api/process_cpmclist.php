<?php
require_once 'initialize.php';
$table = 'cpmc_info';
$res = '';

$data = json_decode($_POST['data']);
$prev_name = $data->filename;
$id = $data->id;
$upload_path = "../uploads/ekscda/";

if ($prev_name) {
    @unlink($upload_path . $prev_name);
}

$extension = strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));
$filename = uuid() . "." . $extension;
$filepath = $upload_path . $filename;
if ($res = move_uploaded_file($_FILES['file']['tmp_name'], $filepath)) {

    if(updateRecord($table, "CPMCLists=:CPMCLists", "id=:id", ['id' => $id, 'CPMCLists'=>$filename])) {
    echo json_encode(['filename' => $filename]);
    } else {        
        echo json_encode('Unable To Upload File');
    }
} else {
    echo json_encode('Unable To Upload File');
}
