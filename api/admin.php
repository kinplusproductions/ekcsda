<?php
require 'initialize.php';

$method = $_SERVER['REQUEST_METHOD'];
$table = 'admin';
$json_fields = [];
$alladmins = [];
$res = '';

switch ($method) {
    case 'GET':
        $u = isset($_GET['u']) ? $_GET['u'] : '';
        $count = isset($_GET['count']) ? $_GET['count'] : '';
        if ($u == 2) {
            if ($count == 1) {
                echo json_encode(countRecords($table));
            } else {
                $res = selectRecords($table, [], "1 ORDER BY created_on");
                $allroutes = [];
                foreach ($res as $val) {
                    $admin_id = $val['id'];
                    $routes = selectRecords('route_access', [], "admin_id=:admin_id", ['admin_id'=>$admin_id], "route_id");
                    $val['RouteSelected'] = $routes;
                    $alladmins[] = $val;
                }
                echo json_encode($alladmins);
            }
        } else {
            echo json_encode([]);
        }
        break;
    case 'POST':
        $data = json_decode(file_get_contents("php://input"), true);
        $Username = $data['Username'];
        $Fullname = $data['Fullname'];
        $res2 = selectRecord($table, [], "Username=:Username OR Fullname=:Fullname", ['Username' => $Username, 'Fullname' => $Fullname]);
        if ($res2) {
            if ($Username == $res2['Username']) {
                echo json_encode('A User With This Username Already Exists');
            } elseif ($Fullname == $res2['Fullname']) {
                echo json_encode('A User With This Name Already Exists');
            }
            return;
        }
        $id = $data['id'] = uuid();
        $column = "id, Fullname, Position, Passwd, AdminLevel, Username, AdminStatus, created_by, modified_by";
        $value = ":id, :Fullname, :Position, :Passwd, :AdminLevel, :Username, :AdminStatus, :created_by, :modified_by";

        $res = insertRecord($table, $column, $value, $data);

        echo $res ? json_encode(selectRecord($table, [], "id=:id", ['id'=>$id], "id")) : json_encode('Unable to create new record');
        break;

    case 'PUT':
    case 'PATCH':
        $data = json_decode(file_get_contents("php://input"), true);
        $res = '';
        extract($data);
        switch ($type) {
            case 'admin':
                $column = "Fullname=:Fullname, Position=:Position, Passwd=:Passwd, AdminLevel=:AdminLevel, Username=:Username, AdminStatus=:AdminStatus, modified_by=:modified_by";
                $update_data = ['id' => $id, 'Fullname' => $Fullname, 'Position' => $Position, 'Passwd' => $Passwd, 'AdminLevel' => $AdminLevel, 'Username' => $Username, 'AdminStatus' => $AdminStatus, 'modified_by'=>$modified_by];
                $res = updateRecord($table, $column, "id=:id", $update_data);
                break;
            default:
                break;
        }

        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;

    case 'DELETE':
        $id = $_SERVER['QUERY_STRING'];
        $res = deleteRecord($table, "id=:id", ['id' => $id]);
        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;
    default:
        break;
}
