<?php
require 'initialize.php';

$method = $_SERVER['REQUEST_METHOD'];
$table = 'cdps_info';
$json_fields = [];
$res = '';

switch ($method) {
    case 'GET':
        $u = isset($_GET['u']) ? $_GET['u'] : '';
        $count = isset($_GET['count']) ? $_GET['count'] : '';

        if ($u == 2) {
            if ($count == 1) {
                echo json_encode(countRecords($table));
            } else {
                echo json_encode(selectRecords($table, [], "1 ORDER BY created_on"));
            }
        } else {
            echo json_encode([]);
        }
        break;
    case 'POST':
        $data = json_decode(file_get_contents("php://input"), true);
        $CDPRef = $data['CDPRef'];
        $res2 = selectRecord($table, [], "CDPRef=:CDPRef ", ['CDPRef' => $CDPRef]);
        if ($res2) {
            if ($CDPRef == $res2['CDPRef']) {
                echo json_encode('This Ref Number Already Exists');
            } 
            return;
        }
        $id = $data['id'] = uuid();
        $column = "id, CDPRef, GroupName, projects, sentization, AssessmentDate, ElectionDate, DeskDate, ReviewDate, SubmissionDate, FieldDate, ApprovalDate, TrainingDate, LaunchDate, CDPTotalCost, CDPStatus, CompletedDate, ApprovedMPs1, ApprovedMPs2, ApprovedMPs3, CdpComment, created_by, modified_by";
        $value = ":id, :CDPRef, :GroupName, :projects, :sentization, :AssessmentDate, :ElectionDate, :DeskDate, :ReviewDate, :SubmissionDate, :FieldDate, :ApprovalDate, :TrainingDate, :LaunchDate, :CDPTotalCost, :CDPStatus, :CompletedDate, :ApprovedMPs1, :ApprovedMPs2, :ApprovedMPs3, :CdpComment, :created_by, :modified_by";

        $res = insertRecord($table, $column, $value, $data);

        echo $res ? json_encode(['ok' => 1]) : json_encode('Unable to create new record');
        break;

    case 'PUT':
    case 'PATCH':
        $data = json_decode(file_get_contents("php://input"), true);
        $res = '';
        extract($data);
        switch ($type) {
            case 'cdps':
                $column = "CDPRef=:CDPRef, GroupName=:GroupName, projects=:projects, sentization=:sentization, AssessmentDate=:AssessmentDate, ElectionDate=:ElectionDate, DeskDate=:DeskDate, ReviewDate=:ReviewDate, SubmissionDate=:SubmissionDate, FieldDate=:FieldDate, ApprovalDate=:ApprovalDate, TrainingDate=:TrainingDate, LaunchDate=:LaunchDate, CDPTotalCost=:CDPTotalCost, CDPStatus=:CDPStatus, CompletedDate=:CompletedDate, ApprovedMPs1=:ApprovedMPs1, ApprovedMPs2=:ApprovedMPs2, ApprovedMPs3=:ApprovedMPs3, 
                CdpComment=:CdpComment, created_by=:created_by, modified_by=:modified_by";
                $update_data = ['id' => $id, 'CDPRef'=>$CDPRef, 'GroupName'=>$GroupName, 'projects'=>$projects, 'sentization'=>$sentization, 'AssessmentDate'=>$AssessmentDate, 'ElectionDate'=>$ElectionDate, 'DeskDate'=>$DeskDate, 'ReviewDate'=>$ReviewDate, 'SubmissionDate'=>$SubmissionDate, 'FieldDate'=>$FieldDate, 'ApprovalDate'=>$ApprovalDate, 'TrainingDate'=>$TrainingDate, 'LaunchDate'=>$LaunchDate, 'CDPTotalCost'=>$CDPTotalCost, 'CDPStatus'=>$CDPStatus, 'CompletedDate'=>$CompletedDate, 'ApprovedMPs1'=>$ApprovedMPs1, 'ApprovedMPs2'=>$ApprovedMPs2, 'ApprovedMPs3'=>$ApprovedMPs3, 'CdpComment'=>$CdpComment, 'created_by'=>$created_by, 'modified_by'=>$modified_by];
                $res = updateRecord($table, $column, "id=:id", $update_data);
                break;
            default:
                break;
        }

        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;

    case 'DELETE':
        $id = $_SERVER['QUERY_STRING'];
        $res = deleteRecord($table, "id=:id", ['id' => $id]);
        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;
    default:
        break;
}
