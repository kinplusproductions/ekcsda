<?php
require 'initialize.php';

$method = $_SERVER['REQUEST_METHOD'];
$table = 'mps_info';
$json_fields = [];
$res = '';

switch ($method) {
    case 'GET':
        $u = isset($_GET['u']) ? $_GET['u'] : '';
        $count = isset($_GET['count']) ? $_GET['count'] : '';

        if ($u == 2) {
            if ($count == 1) {
                echo json_encode(countRecords($table));
            } else {
                echo json_encode(selectRecords($table, [], "1 ORDER BY created_on"));
            }
        } else {
            echo json_encode([]);
        }
        break;
    case 'POST':
        $data = json_decode(file_get_contents("php://input"), true);
        $MPsRefNo = $data['MPsRefNo'];
        $res2 = selectRecord($table, [], "MPsRefNo=:MPsRefNo ", ['MPsRefNo' => $MPsRefNo]);
        if ($res2) {
            if ($MPsRefNo == $res2['MPsRefNo']) {
                echo json_encode('This Ref Number Already Exists');
            } 
            return;
        }
        $id = $data['id'] = uuid();
        $column = "id, MPsRefNo, MPsType, MPsApproved, AgencyContribution, CommunityContribution, MPsTotalCost, Sector, CordinatesLat, CordinatesLong, FirstTranche, FirstTrancheDate, SecondTranche, SecondTrancheDate, ThirdTranche, ThirdTrancheDate, FundNotYetRelase, CommenceDate, Status, mpsComment, created_by, modified_by";
        $value = ":id, :MPsRefNo, :MPsType, :MPsApproved, :AgencyContribution, :CommunityContribution, :MPsTotalCost, :Sector, :CordinatesLat, :CordinatesLong, :FirstTranche, :FirstTrancheDate, :SecondTranche, :SecondTrancheDate, :ThirdTranche, :ThirdTrancheDate, :FundNotYetRelase, :CommenceDate, :Status, :mpsComment, :created_by, :modified_by";

        $res = insertRecord($table, $column, $value, $data);

        echo $res ? json_encode(['ok' => 1]) : json_encode('Unable to create new record');
        break;

    case 'PUT':
    case 'PATCH':
        $data = json_decode(file_get_contents("php://input"), true);
        $res = '';
        extract($data);
        switch ($type) {
            case 'mps':
                $column = "MPsRefNo=:MPsRefNo, MPsType=:MPsType, MPsApproved=:MPsApproved, AgencyContribution=:AgencyContribution, CommunityContribution=:CommunityContribution, MPsTotalCost=:MPsTotalCost, Sector=:Sector, CordinatesLat=:CordinatesLat, CordinatesLong=:CordinatesLong, FirstTranche=:FirstTranche, FirstTrancheDate=:FirstTrancheDate, SecondTranche=:SecondTranche, SecondTrancheDate=:SecondTrancheDate, ThirdTranche=:ThirdTranche, ThirdTrancheDate=:ThirdTrancheDate, FundNotYetRelase=:FundNotYetRelase, CommenceDate=:CommenceDate, Status=:Status, mpsComment=:mpsComment, created_by=:created_by, modified_by=:modified_by";

                $update_data = ['id' => $id, 'MPsRefNo'=>$MPsRefNo, 'MPsType'=>$MPsType, 'MPsApproved'=>$MPsApproved, 'AgencyContribution'=>$AgencyContribution, 'CommunityContribution'=>$CommunityContribution, 'MPsTotalCost'=>$MPsTotalCost, 'Sector'=>$Sector, 'CordinatesLat'=>$CordinatesLat,
                'CordinatesLong'=>$CordinatesLong, 'FirstTranche'=>$FirstTranche, 'FirstTrancheDate'=>$FirstTrancheDate, 'SecondTranche'=>$SecondTranche, 'SecondTrancheDate'=>$SecondTrancheDate, 'ThirdTranche'=>$ThirdTranche, 'ThirdTrancheDate'=>$ThirdTrancheDate, 'FundNotYetRelase'=>$FundNotYetRelase, 'CommenceDate'=>$CommenceDate, 'Status'=>$Status, 
                'mpsComment' => $mpsComment,
                'created_by'=>$created_by, 'modified_by'=>$modified_by];
                $res = updateRecord($table, $column, "id=:id", $update_data);
                break;
            default:
                break;
        }

        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;

    case 'DELETE':
        $id = $_SERVER['QUERY_STRING'];
        $res = deleteRecord($table, "id=:id", ['id' => $id]);
        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;
    default:
        break;
}
