<?php
require 'initialize.php';

$method = $_SERVER['REQUEST_METHOD'];
$table = 'route_access';
$json_fields = [];
$res = '';

switch ($method) {
    case 'GET':
        $u = isset($_GET['u']) ? $_GET['u'] : '';
        $count = isset($_GET['count']) ? $_GET['count'] : '';
        $admin_id = isset($_GET['admin_id']) ? $_GET['admin_id'] : '';

        if ($u == 3) {
            echo json_encode(selectRecords($table, []));
        } else if ($u == 2) {
            if ($count == 1) {
                echo json_encode(countRecords($table));
            } else {
                $table2 = "admin";
                $column = "t1.id, t1.admin_id, t1.route_id, t2.id";
                $on_clause = "t1.admin_id=t2.id";
                $where_clause = "t1.admin_id=:admin_id ORDER BY t2.admin_id";
                $data = ['admin_id' => $admin_id];

                echo json_encode(joinRecords($table, $table2, $column, $on_clause, $where_clause, $json_fields, $data));

            }
        } else if ($u == 1) {
                echo json_encode(selectRecords($table, [], "admin_id=:admin_id", ['admin_id'=>$admin_id], "id"));
        }
        else {
            echo json_encode([]);
        }
        break;
    case 'POST':
        $data = json_decode(file_get_contents("php://input"), true);
        extract($data);
        if (!empty($RouteSelected)) {
            foreach ($RouteSelected as $route) {
                $id = uuid();
                $route_id = $route;
                $post_data = ['id' => $id, 'admin_id' => $admin_id, 'route_id' => $route_id];
                $column = "id,admin_id,route_id";
                $value = ":id, :admin_id, :route_id";
                $res = insertRecord($table, $column, $value, $post_data);
            }
            echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        } else {
            echo json_encode(['ok' => 1]);
        }
        
        
        break;

    case 'PUT':
        $data = json_decode(file_get_contents("php://input"));
        foreach ($data as $id) {
            $res = deleteRecord($table, "id=:id", ['id' => $id]);
        }
        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;
    case 'PATCH':
        $data = json_decode(file_get_contents("php://input"), true);
        extract($data);
        foreach ($RouteSelected as $route_id) {
            $res = deleteRecord($table, "route_id=:route_id AND admin_id=:admin_id", ['route_id' => $route_id, 'admin_id'=>$admin_id]);
        }
        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;

    case 'DELETE':
        $id = $_SERVER['QUERY_STRING'];
        $res = deleteRecord($table, "id=:id", ['id' => $id]);
        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;

    default:
        break;
}
