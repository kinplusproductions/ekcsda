<?php
require 'initialize.php';

$method = $_SERVER['REQUEST_METHOD'];
$table = 'community_info';
$json_fields = [];
$res = '';

switch ($method) {
    case 'GET':
        $u = isset($_GET['u']) ? $_GET['u'] : '';
        $count = isset($_GET['count']) ? $_GET['count'] : '';

        if ($u == 2) {
            if ($count == 1) {
                echo json_encode(countRecords($table));
            } else {
                echo json_encode(selectRecords($table, [], "1 ORDER BY created_on"));
            }
        } else {
            echo json_encode([]);
        }
        break;
    case 'POST':
        $data = json_decode(file_get_contents("php://input"), true);
        $CommunityRefNo = $data['CommunityRefNo'];
        $res2 = selectRecord($table, [], "CommunityRefNo=:CommunityRefNo ", ['CommunityRefNo' => $CommunityRefNo]);
        if ($res2) {
            if ($CommunityRefNo == $res2['CommunityRefNo']) {
                echo json_encode('This Ref Number Already Exists');
            } 
            return;
        }
        $id = $data['id'] = uuid();
        $column = "id, CommunityRefNo, CommunityName, LGA, Ward, Population, CommunityMaleNo, CommunityFemaleNo, SocialGroup, Beneficiaries, created_by, modified_by";
        $value = ":id, :CommunityRefNo, :CommunityName, :LGA, :Ward, :Population, :CommunityMaleNo, :CommunityFemaleNo, :SocialGroup, :Beneficiaries, :created_by, :modified_by";

        $res = insertRecord($table, $column, $value, $data);

        echo $res ? json_encode(selectRecord($table, [], "id=:id", ['id'=>$id])) : json_encode('Unable to create new record');
       
        break;

    case 'PUT':
    case 'PATCH':
        $data = json_decode(file_get_contents("php://input"), true);
        $res = '';
        extract($data);
        switch ($type) {
            case 'community':
                $column = "CommunityRefNo=:CommunityRefNo, CommunityName=:CommunityName, LGA=:LGA, Ward=:Ward, Population=:Population, CommunityMaleNo=:CommunityMaleNo, CommunityFemaleNo=:CommunityFemaleNo, SocialGroup=:SocialGroup, Beneficiaries=:Beneficiaries, created_by=:created_by, modified_by=:modified_by";

                $update_data = ['id' => $id, 'CommunityRefNo'=>$CommunityRefNo, 'CommunityName'=>$CommunityName, 'LGA'=>$LGA, 'Ward'=>$Ward, 'Population'=>$Population, 'CommunityMaleNo'=>$CommunityMaleNo, 'CommunityFemaleNo'=>$CommunityFemaleNo, 'SocialGroup'=>$SocialGroup, 'Beneficiaries'=>$Beneficiaries, 'created_by'=>$created_by, 'modified_by'=>$modified_by];
                
                $res = updateRecord($table, $column, "id=:id", $update_data);
                break;
            default:
                break;
        }

        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;

    case 'DELETE':
        $id = $_SERVER['QUERY_STRING'];
        extract(selectRecord($table, $json_fields, "id= :id", ['id'=>$id], "*"));
        $upload_path = "../uploads/ekscda/";
        
		$Beneficiaries ? @unlink($upload_path . $Beneficiaries): '';

        $res = deleteRecord($table, "id=:id", ['id' => $id]);
        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;
    default:
        break;
}
