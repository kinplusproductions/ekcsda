<?php 
require 'initialize.php';

$table = "admin";
$alladmins = [];
$data = json_decode(file_get_contents("php://input"), true); // Get raw posted data
extract($data);

$res = selectRecord($table, [], "Username=:Username AND Passwd=:Passwd", ['Username' => $Username, 'Passwd' => $Passwd]);

if ($res) {
	if ($res['AdminStatus'] == 1) {
		$allroutes= [];
			$admin_id = $res['id'];
			$routes = selectRecords('route_access', [], "admin_id=:admin_id", ['admin_id'=>$admin_id], "route_id");
			foreach ($routes as $route) {
				$allroutes[] = $route['route_id'];
			}
			$res['RouteSelected'] = $allroutes;
			$alladmins = $res;
		echo json_encode($alladmins);
	} else {
		echo json_encode('You are currently deactivated on this platform') ;
	}	
}else{
	echo json_encode('Incorrect Login Details: Please Enter Correct Details');
}

?>