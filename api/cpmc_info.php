<?php
require 'initialize.php';

$method = $_SERVER['REQUEST_METHOD'];
$table = 'cpmc_info';
$json_fields = [];
$res = '';

switch ($method) {
    case 'GET':
        $u = isset($_GET['u']) ? $_GET['u'] : '';
        $count = isset($_GET['count']) ? $_GET['count'] : '';

        if ($u == 2) {
            if ($count == 1) {
                echo json_encode(countRecords($table));
            } else {
                echo json_encode(selectRecords($table, [], "1 ORDER BY created_on"));
            }
        } else {
            echo json_encode([]);
        }
        break;
    case 'POST':
        $data = json_decode(file_get_contents("php://input"), true);
        $CPMCRefNo = $data['CPMCRefNo'];
        $res2 = selectRecord($table, [], "CPMCRefNo=:CPMCRefNo ", ['CPMCRefNo' => $CPMCRefNo]);
        if ($res2) {
            if ($CPMCRefNo == $res2['CPMCRefNo']) {
                echo json_encode('This Ref Number Already Exists');
            } 
            return;
        }
        $id = $data['id'] = uuid();
        $column = "id, CPMCRefNo, Committee, NumberOfCpmc, CPMCMaleNo, PercentOfMale, CPMCFemaleNo, PercentOfFemale, Vulnerability, RegDate, CPMCLists, created_by, modified_by";
        $value = ":id, :CPMCRefNo, :Committee, :NumberOfCpmc, :CPMCMaleNo, :PercentOfMale, :CPMCFemaleNo, :PercentOfFemale, :Vulnerability, :RegDate, :CPMCLists, :created_by, :modified_by";

        $res = insertRecord($table, $column, $value, $data);

        echo $res ? json_encode(selectRecord($table, [], "id=:id", ['id'=>$id])) : json_encode('Unable to create new record');
        break;

    case 'PUT':
    case 'PATCH':
        $data = json_decode(file_get_contents("php://input"), true);
        $res = '';
        extract($data);
        switch ($type) {
            case 'cpmcs':
                $column = "CPMCRefNo=:CPMCRefNo, Committee=:Committee, NumberOfCpmc=:NumberOfCpmc, CPMCMaleNo=:CPMCMaleNo, PercentOfMale=:PercentOfMale, CPMCFemaleNo=:CPMCFemaleNo, PercentOfFemale=:PercentOfFemale, Vulnerability=:Vulnerability, RegDate=:RegDate, CPMCLists=:CPMCLists, created_by=:created_by, modified_by=:modified_by";
                $update_data = ['id' => $id, 'CPMCRefNo'=>$CPMCRefNo, 'Committee'=>$Committee, 'NumberOfCpmc'=>$NumberOfCpmc, 'CPMCMaleNo'=>$CPMCMaleNo, 'PercentOfMale'=>$PercentOfMale, 'CPMCFemaleNo'=>$CPMCFemaleNo, 'PercentOfFemale'=>$PercentOfFemale, 'Vulnerability'=>$Vulnerability, 'RegDate'=>$RegDate, 'CPMCLists'=>$CPMCLists, 'created_by'=>$created_by, 'modified_by'=>$modified_by];
                $res = updateRecord($table, $column, "id=:id", $update_data);
                break;
            default:
                break;
        }

        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;

    case 'DELETE':
        $id = $_SERVER['QUERY_STRING'];

		extract(selectRecord($table, $json_fields, "id= :id", ['id'=>$id], "*"));
        $upload_path = "../uploads/ekscda/";
        
		$CPMCLists ? @unlink($upload_path . $CPMCLists): '';

        $res = deleteRecord($table, "id=:id", ['id' => $id]);
        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;
    default:
        break;
}
