<?php
require_once 'initialize.php';

$method = $_SERVER['REQUEST_METHOD'];
$res = '';
$table = 'route_tb';
$json_fields = [];
$marks = [];

switch ($method) {
    case 'GET':
        $u = isset($_GET['u']) ? $_GET['u'] : '';
        $count = isset($_GET['count']) ? $_GET['count'] : '';
        $type = isset($_GET['type']) ? $_GET['type'] : '';
        $id = isset($_GET['id']) ? $_GET['id'] : '';

        if ($u == 1) {
            if ($id == '') {
            echo json_encode([]);
            } else {
            echo json_encode(selectRecord($table, $json_fields, 'id=:id', ['id' => $id], "id, title, link, type"));
            }
        } elseif ($u == 2) {
            if ($count == 1) {
                echo json_encode(countRecords($table));
                return;
            } elseif ($type == 'forms') {                
                echo json_encode(selectRecords($table, $json_fields, 'type=:type', ['type' => $type], "id, title, link"));
            } elseif ($type == 'tables') {
                echo json_encode(selectRecords($table, $json_fields, 'type=:type', ['type' => $type], "id, title, link"));
            }
        } else {
            echo json_encode([]);
        }
        break;
}
