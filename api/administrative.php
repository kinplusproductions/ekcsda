<?php
require 'initialize.php';

$method = $_SERVER['REQUEST_METHOD'];
$table = 'administrative';
$json_fields = [];
$res = '';

switch ($method) {
    case 'GET':
        $u = isset($_GET['u']) ? $_GET['u'] : '';
        $count = isset($_GET['count']) ? $_GET['count'] : '';

        if ($u == 2) {
            if ($count == 1) {
                echo json_encode(countRecords($table));
            } else {
                echo json_encode(selectRecords($table, [], "1 ORDER BY created_on"));
            }
        } else {
            echo json_encode([]);
        }
        break;
    case 'POST':
        $data = json_decode(file_get_contents("php://input"), true);
        $id = $data['id'] = uuid();
        $column = "id, MeetingType, Meeting, Date, Attendee, IVA, Sensitization, created_by, modified_by";
        $value = ":id, :MeetingType, :Meeting, :Date, :Attendee, :IVA, :Sensitization, :created_by, :modified_by";

        $res = insertRecord($table, $column, $value, $data);

        echo $res ? json_encode(['ok' => 1]) : json_encode('Unable to create new record');
        break;

    case 'PUT':
    case 'PATCH':
        $data = json_decode(file_get_contents("php://input"), true);
        $res = '';
        extract($data);
        switch ($type) {
            case 'administrative':
                $column = "MeetingType=:MeetingType, Meeting=:Meeting, Date=:Date, Attendee=:Attendee, IVA=:IVA, Sensitization=:Sensitization, created_by=:created_by, modified_by=:modified_by";
                $update_data = ['id' => $id, 'MeetingType' => $MeetingType, 'Meeting' => $Meeting, 'Date' => $Date, 'Attendee' => $Attendee, 'IVA' => $IVA, 'Sensitization' => $Sensitization, 'created_by' => $created_by, 'modified_by' => $modified_by];
                $res = updateRecord($table, $column, "id=:id", $update_data);
                break;
            default:
                break;
        }

        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;

    case 'DELETE':
        $id = $_SERVER['QUERY_STRING'];
        $res = deleteRecord($table, "id=:id", ['id' => $id]);
        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;
    default:
        break;
}
